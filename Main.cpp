#include <iostream>
#include <time.h>

int main()
{
	const int N = 10;
	int MyArray[N][N];
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			MyArray[i][j] = i + j;
			std::cout << '\t';
			std::cout << MyArray[i][j];
		}
		std::cout << '\n';
	}

	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	int CurrDay = buf.tm_mday;

	int ArrSum = 0;
	for (int j = 0; j < N; j++)
	{
		ArrSum += MyArray[CurrDay % N][j];

	}
	std::cout << '\n';
	std::cout << "The sum of the elements in " << CurrDay % N << "th string = " << ArrSum << '\n';
}